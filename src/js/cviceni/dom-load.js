import idleTimer from './idle-timer';
import Cviceni from './cviceni';
import Navigation from './navigation';
import Slides from './slides';
import SaveData from './save-data';
import Analytics from './analytics';
import Forms from './forms';

import polyfillNodelistForeach from '../lib/polyfill-nodelist-foreach';

// Edge doesn't support:
// .forEach() on NodeLists
polyfillNodelistForeach();

const domLoad = () => {
  console.log('domLoad:cviceni');

  const $root = document.documentElement;

  // instantiate
  HISTORYLAB.analytics = new Analytics();
  const cviceni = new Cviceni();
  const navigation = new Navigation();
  const slides = new Slides();
  const data = new SaveData();
  const forms = new Forms();

  // run
  navigation
    .loopNavButtons();

  slides
    .slides();

  data
    .checkForData(HISTORYLAB.import.exerciseData || {})
    .init();

  cviceni
    .header();

  forms
    .init();

  idleTimer({
    // function to fire after idle
    callback: () => $root.classList.add('is-idle'),
    // function to fire when active
    activeCallback: () => $root.classList.remove('is-idle'),
    // Amount of time in milliseconds before becoming idle. default 60000
    idleTime: 3000,
  });

  // if (HISTORYLAB.import.done) {
  //   const $srcs = document.querySelectorAll('audio, video, source, img, image');
  //   const replace = ((src) => src.replace('/cviceni-aktualni', '..'));

  //   $srcs.forEach(($src) => {
  //     switch ($src.nodeName) {
  //       case 'AUDIO':
  //       case 'IMG':
  //       case 'SOURCE':
  //       case 'VIDEO':
  //         $src.setAttribute(
  //           'src',
  //           replace($src.getAttribute('src')),
  //         );

  //         break;

  //       case 'image':
  //         $src.setAttribute(
  //           'xlink:href',
  //           replace($src.getAttribute('xlink:href')),
  //         );
  //         break;

  //       default:
  //         break;
  //     }
  //   });
  // }
};

export default domLoad;
